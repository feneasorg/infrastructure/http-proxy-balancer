package main
//
// Http Proxy Balancer - This proxy can split traffic across multiple child proxies
// Copyright (C) 2020  Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
import (
  "log"
  "net"
  "net/http"
  "net/url"
  "flag"
  "fmt"
  "strings"
  "sync"
  "time"

  "github.com/elazarl/goproxy"
)

var verbose, relay bool
var relayAddresses, listeningAddress, lookupHost string
var lookupHostPort int

func init() {
  flag.BoolVar(&verbose, "verbose", false, "Receive verbose log messages")
  flag.BoolVar(&relay, "relay", false, "Should this proxy act as a relay")
  flag.StringVar(&relayAddresses, "relay-address", "",
    "The addresses where content should be relayed to (separated by comma)")
  flag.StringVar(&lookupHost, "lookup-host", "",
    "The proxy can lookup CNAME records by a specified host")
  flag.IntVar(&lookupHostPort, "lookup-host-port", 8081,
    "The port which is being used for every looked up host")
  flag.StringVar(&listeningAddress, "listening-address", ":8080",
    "On which address and port should the proxy run")
}

func main() {
  flag.Parse()
  if relay && (relayAddresses == "" && lookupHost == ""){
    flag.PrintDefaults()
    log.Fatal("If the relay is being used the -relay-address should be set too")
  }

  proxy := goproxy.NewProxyHttpServer()
  proxy.Verbose = verbose

  if relay {
    var addresses = parseAddresses()
    go func() {
      for {
        time.Sleep(time.Second * 60)
        addresses = parseAddresses()
        log.Println(fmt.Sprintf("Updating the following child proxies: %+v", addresses))
      }
    }()

    log.Println("Acting as relay")
    if verbose {
      log.Println(fmt.Sprintf("Using the following child proxies: %+v", addresses))
    }

    var counter = struct{
      Count int
      Mutex sync.Mutex
    }{}

    // override the existing connect method to dynamically switch addresses
    connectDial := func(network string, addr string) (net.Conn, error) {
      counter.Mutex.Lock()
      defer func() {
        counter.Count++
        counter.Mutex.Unlock()
      }()

      if counter.Count >= len(addresses) {
        counter.Count = 0
      }

      // return the new address with the proxy call as well
      proxy.Tr.Proxy = func(req *http.Request) (*url.URL, error) {
        url, err := url.Parse(addresses[counter.Count])
        if err != nil {
          log.Println(err.Error())
        }
        return url, err
      }

      log.Printf("using %s", addresses[counter.Count])
      conn, err := proxy.NewConnectDialToProxy(addresses[counter.Count])(network, addr)
      if err != nil {
        log.Println(err.Error())
      }
      return conn, err
    }
    proxy.ConnectDial = connectDial
  } else {
    log.Println("Acting as proxy")
  }

  log.Println(fmt.Sprintf("Listening on %s", listeningAddress))
  log.Fatal(http.ListenAndServe(listeningAddress, proxy))
}

func parseAddresses() (addresses []string) {
  // Lookup IP addresses for a specified DNS address
  if lookupHost != "" {
    result, err := net.LookupHost(lookupHost)
    if err != nil {
      log.Fatal(err.Error())
    }

    for idx, _ := range result {
      addresses = append(addresses, fmt.Sprintf("http://%s:%d", result[idx], lookupHostPort))
    }
  }
  // Use staticly configured addresses
  if relayAddresses != "" {
    for _, address := range strings.Split(relayAddresses, ",") {
      if !strings.HasPrefix(address, "http") {
        address = fmt.Sprintf("http://%s", address)
      }
      if len(strings.Split(address, ":")) < 3 {
        address = fmt.Sprintf("%s:80", address)
      }
      addresses = append(addresses, address)
    }
  }
  return
}
