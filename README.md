# HTTP Proxy Balancer

This proxy balancer was designed to split outgoing traffic across multiple kubernetes nodes.
This is required if the outgoing traffic should not originate from a single IP address.

The reason for this project was the metadata search engine [Searx](https://github.com/asciimoo/searx)
and the constant problem of getting black-listed by search engines like google or bing.

# Usage

```
Usage of ./http-proxy-balancer:
  -listening-address string
        On which address and port should the proxy run (default ":8080")
  -lookup-host string
        The proxy can lookup CNAME records by a specified host
  -lookup-host-port int
        The port which is being used for every looked up host (default 8081)
  -relay
        Should this proxy act as a relay
  -relay-address string
        The addresses where content should be relayed to (separated by comma)
  -verbose
        Receive verbose log messages
```

# Example workflow

![diagram.png](diagram.png)
