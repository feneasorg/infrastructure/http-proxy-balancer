FROM golang:1.14
COPY . /go/src/http-proxy-balancer
WORKDIR /go/src/http-proxy-balancer
RUN go build -o proxy

FROM alpine:3.12
COPY --from=0 /go/src/http-proxy-balancer/proxy /usr/bin/proxy
RUN chmod +x /usr/bin/proxy
# NOTE fix golang binaries for alpine
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2
ENTRYPOINT ["/usr/bin/proxy"]
